﻿using UnityEngine;
using System.Collections;

public class balloon_spawner : MonoBehaviour {
    public int initial_canvas_timer;
    public static int correct_hit_reward = 2; // 2 seconds added to the timer whenever a balloon is popped
    public static int canvas_timer = 30;
    public static int canvas_score = 0;

    public GameObject[] balloons;
    public float spawn_timer; // speed of spawning
    float t; // timer for spawning balloon
    float difficulty_timer; // subtract .01 from the spawn_timer to make it spawn faster, increasing the difficulty
    float t1; // The timer too call every one second

    float screen_width;
    float screen_height;

	// Use this for initialization
	void Start () {
        t = Time.time;
        canvas_timer = initial_canvas_timer;
        screen_width = Mathf.Abs(Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, 0, 0)).x);
        screen_height = Mathf.Abs(Camera.main.ScreenToWorldPoint(new Vector3(Screen.height, 0, 0)).y);
    }
	
	// Update is called once per frame
	void Update () {
        StartCoroutine(spawn_balloons());

        if (Time.time - difficulty_timer > 10 && spawn_timer > .3f)
        {
            spawn_timer -= .01f;
            difficulty_timer = Time.time;
        }

        if (Time.time - t1 > 1)
        {
            t1 = Time.time;
            canvas_timer -= 1;
            canvas_score += 1;
        }

        if (canvas_timer < 0)
        {
            canvas_timer = initial_canvas_timer;
            canvas_score = 0;
            Application.LoadLevel("level1");
        }
    }
    
    IEnumerator spawn_balloons()
    {
        if (Time.time - t > spawn_timer)
        {
            t = Time.time;
            for (int i = 0; i < 1; i++)
            {
                Instantiate(balloons[Random.Range(0, 6)], new Vector3(Random.Range(-screen_width + 1, screen_width - 1), -screen_height - 2, 0), Quaternion.identity);
                yield return new WaitForSeconds(.1f);
            }
        }
    }
}

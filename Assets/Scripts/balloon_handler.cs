﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class balloon_handler : MonoBehaviour {
    public AudioClip clip; // Sound of balloon popping
    public AudioSource audio ;

    Vector3 speed;
    string target_balloon_color;
    Renderer renderer;
    
    // Use this for initialization
    void Start () {
        speed = new Vector3(0,.1f,0);
        target_balloon_color = "red";

        renderer = gameObject.GetComponent<Renderer>();
    }

    // Update is called once per frame
    bool seen = false;
	void Update () {
        this.transform.position += speed;

        // Destroy object if it moves out of view
        if (renderer.isVisible)
            seen = true;
        if (seen && !renderer.isVisible )
            Destroy(gameObject);

        RaycastHit hit = new RaycastHit();
        for (int i = 0; i < Input.touchCount; ++i)
        {
            if (Input.GetTouch(i).phase.Equals(TouchPhase.Began))
            {
                // Construct a ray from the current touch coordinates
                Ray ray = Camera.main.ScreenPointToRay(Input.GetTouch(i).position);
                if (Physics.Raycast(ray, out hit))
                {
                    hit.transform.gameObject.SendMessage("OnMouseDown");
                }

            }
        }
    }

    private Plane plane;
    private Vector3 v3Offset;
    void OnMouseDown()
    {
        if (name.Contains("balloon_" + target_balloon_color)) // only destroy the target balloons
        { 
            // add 1 to the countdown timer if we hit a target balloon
            balloon_spawner.canvas_timer += balloon_spawner.correct_hit_reward;

            // Play blop sound
            audio = Instantiate<AudioSource>(audio);
            StartCoroutine(plz_destroy());
        }
    }
    
    IEnumerator plz_destroy()
    {
        Destroy(gameObject);
        while (audio.isPlaying)
        {
            yield return new WaitForSeconds(1);
        }
        Destroy(audio); 
    }
    void OnCollisionEnter2D(Collision2D other)
    {
       // Destroy(this.gameObject);
    }
}

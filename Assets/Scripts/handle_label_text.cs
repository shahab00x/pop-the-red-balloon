﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class handle_label_text : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        if (name.Contains("Timer"))
            GetComponent<Text>().text = balloon_spawner.canvas_timer.ToString();
        else if (name.Contains("Score"))
            GetComponent<Text>().text = balloon_spawner.canvas_score.ToString();
    }
}
